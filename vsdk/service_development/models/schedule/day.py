from django.db import models
from .prescription import Prescription
from ..viewonly import ViewPermissionsMixin

class Day(ViewPermissionsMixin):
    day_number = models.IntegerField()
    prescriptions = models.ManyToManyField(Prescription)

    class Meta(ViewPermissionsMixin.Meta):
        abstract = False