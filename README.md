# KasaDaka-Voice Service Development Kit

Ubuntu pico2wav TTS:
install:
	sudo apt-get install libttspico-utils

run:
	pico2wave -w=test.wav -l=en-US foo
or read from file (change en-US for other language, like fr-FR):
	cat input_file.txt | xargs -I foo -0 pico2wave -w=test.wav -l=en-US foo