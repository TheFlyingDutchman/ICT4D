from django.db import models
from .viewonly import *

# based on answer from:
# https://stackoverflow.com/questions/33206304/why-does-django-not-have-a-view-permission

class ViewPermissionsMixin(models.Model):
    """
        Mixin adds view permission to model.
    """
    class Meta:
        abstract=True
        default_permissions = ('add', 'change', 'delete', 'view')