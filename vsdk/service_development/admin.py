from django.contrib import admin

from .models import VoiceService, MessagePresentation, Choice, ChoiceOption, VoiceFragment, CallSession, CallSessionStep, KasaDakaUser, Language, VoiceLabel, Record, Result, Reminder
from .models.schedule import Day, Prescription

def format_validation_result(obj):
        """
        Creates a HTML list from all errors found in validation
        """
        return '<br/>'.join(obj.validator())


# based on answer from:
# https://stackoverflow.com/questions/33206304/why-does-django-not-have-a-view-permission
class AdminViewMixin(admin.ModelAdmin):

    def has_perm(self,user,permission):
        """
            Usefull shortcut for `user.has_perm()`
        """
        if user.has_perm("%s.%s_%s" % (self.model._meta.app_label,permission,self.model.__name__.lower(),)):
            return True
        return False

    def has_module_permission(self, request): # Django 1.8
        return True

    def has_change_permission(self, request, obj=None):
        """
            Necessary permission check to let Django show change_form for `view` permissions
        """
        if request.user.is_superuser:
            return True
        elif self.has_perm(request.user,'change'):
            return True
        elif self.has_perm(request.user,'view'):
            return True
        return super(AdminViewMixin, self).has_change_permission(request, obj)

    def get_readonly_fields(self, request, obj=None):
        """
            Turn each model field into read-only for `viewers`
        """
        all_model_fields = []
        for field in self.model._meta.fields:
            # TODO in Django 1.8 use ModelAdmin.get_fields()
            if not field.auto_created \
                and (not hasattr(field,'auto_now_add') or not field.auto_now_add) \
                and (not hasattr(field,'auto_now') or not field.auto_now) \
                :
                all_model_fields.append(field.name)
        if request.user.is_superuser:
            return self.readonly_fields
        elif self.has_perm(request.user,'change'):
            return self.readonly_fields
        elif self.has_perm(request.user,'view'):
            return all_model_fields
        return self.readonly_fields

    def change_view(self, request, object_id, extra_context=None):
        """
            Disable buttons for `viewers` in `change_view`
        """
        if request.user.is_superuser:
            pass
        elif self.has_perm(request.user,'change'):
            pass
        elif self.has_perm(request.user,'view'):
            extra_context = extra_context or {}
            extra_context['hide_save_buttons'] = True
        return super(AdminViewMixin, self).change_view(request, object_id, extra_context=extra_context)


class VoiceServiceAdmin(admin.ModelAdmin):
    fieldsets = [('General',    {'fields' : ['name', 'description', 'vxml_url', 'active', 'is_valid', 'validation_details', 'supported_languages']}),
                    ('Requirements', {'fields': ['requires_registration']}),
                    ('Call flow', {'fields': ['_start_element']})]
    list_display = ('name','active','is_valid')
    readonly_fields = ('vxml_url', 'is_valid', 'validation_details')

    def get_readonly_fields(self, request, obj=None):
        """
        Only allow activation of voice service if it is valid
        """
        if obj is not None:
            if not obj.is_valid():
                return self.readonly_fields + ('active',)
        return self.readonly_fields


    def validation_details(self, obj=None):
        return format_validation_result(obj)
    validation_details.allow_tags = True


class VoiceServiceElementAdmin(admin.ModelAdmin):
    fieldsets = [('General',    {'fields' : [ 'name', 'description','service','is_valid', 'validation_details', 'voice_label']})]
    list_display = ('name', 'service', 'is_valid')
    readonly_fields = ('is_valid', 'validation_details')

    def validation_details(self, obj=None):
        return format_validation_result(obj)
    validation_details.allow_tags = True

class ChoiceOptionsInline(admin.TabularInline):
    model = ChoiceOption
    extra = 2
    fk_name = 'parent'
    view_on_site = False

class ChoiceAdmin(VoiceServiceElementAdmin):
    inlines = [ChoiceOptionsInline]

class VoiceLabelInline(admin.TabularInline):
    model = VoiceFragment
    extra = 2
    fk_name = 'parent'

class VoiceLabelAdmin(AdminViewMixin):
    inlines = [VoiceLabelInline]

class CallSessionInline(admin.TabularInline):
    model = CallSessionStep
    extra = 0
    fk_name = 'session'
    can_delete = False
    fieldsets = [('General', {'fields' : ['visited_element', 'time']})]
    readonly_fields = ('time','session','visited_element')
    max_num = 0

class KasaDakaUserAdmin(AdminViewMixin):
    # list_display = ('user','caller_id','language')
    # fieldsets = [('General', {'fields' : [ 'phone number', 'first name', 'last name', 'language']})]
    # readonly_fields = ('service','user','caller_id','start','end','language')
    # inlines = [CallSessionInline]
    # can_delete = False
    pass

class CallSessionAdmin(AdminViewMixin):
    list_display = ('start','user','service','caller_id','language')
    fieldsets = [('General', {'fields' : ['service', 'user','caller_id','end']})]
    readonly_fields = ('service','user','caller_id','start','end','language')
    inlines = [CallSessionInline]
    can_delete = False

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, reques, obj=None):
        return False

    def get_actions(self, request):
        actions = super(CallSessionAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

class MessagePresentationAdmin(VoiceServiceElementAdmin):
    fieldsets = VoiceServiceElementAdmin.fieldsets + [('Message Presentation', {'fields': ['_redirect','final_element']})]

class DayAdmin(AdminViewMixin):
    list_display = ['day_number']
    fieldsets = [('Data', {'fields': ['day_number', 'prescriptions']})]

class PrescriptionAdmin(AdminViewMixin):
    list_display = ['get_prescription']

    def get_prescription(self, obj):
        return obj.voice_label.name

    get_prescription.short_description = "prescription voice label"
    get_prescription.admin_order_field  = 'voice_label__name'

class ResultAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name','value','file','session','user')
        }),
        ('Edit', {
            'fields': ('day',),
        }),
    )
    readonly_fields = ['name','value','file','session','user']
    # list_display = ['day']

# Register your models here.
admin.site.register(VoiceService, VoiceServiceAdmin)
admin.site.register(MessagePresentation, MessagePresentationAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(CallSession, CallSessionAdmin)
admin.site.register(KasaDakaUser, KasaDakaUserAdmin)
admin.site.register(Language)
admin.site.register(VoiceLabel, VoiceLabelAdmin)
admin.site.register(Record)
admin.site.register(Day, DayAdmin)
admin.site.register(Prescription, PrescriptionAdmin)
admin.site.register(Result, ResultAdmin)
admin.site.register(Reminder)
