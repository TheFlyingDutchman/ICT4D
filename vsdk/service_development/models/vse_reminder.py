from django.db import models

from vsdk.service_development.models import VoiceLabel
from .vs_element import VoiceServiceElement

class Reminder(VoiceServiceElement):
    """
        An element that plays prescriptions with the recorded name to the user
    """

    _urls_name = 'service-development:reminder'

    prescriptions_for_batch_label = models.ForeignKey(
        VoiceLabel,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='prescriptions_for_batch_label',
    )

    hear_or_skip_label = models.ForeignKey(
        VoiceLabel,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='hear_or_skip_label',
    )

    vaccinate_batch_label = models.ForeignKey(
        VoiceLabel,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='vaccinate_batch_label',
    )

    for_diseases_label = models.ForeignKey(
        VoiceLabel,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='for_diseases_label',
    )

    hear_again_or_continue_label = models.ForeignKey(
        VoiceLabel,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='hear_again_or_continue_label',
    )

    final_label = models.ForeignKey(
        VoiceLabel,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='final_label',
    )

    def __str__(self):
        return "Reminder: " + self.name
