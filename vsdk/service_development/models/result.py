# Credits for this file go to "group 1",
# code from: https://github.com/siaws2001/KasaDaka-VSDK/

from django.db import models
from django.shortcuts import get_object_or_404

from vsdk.service_development.models import CallSession, KasaDakaUser

class Result(models.Model):

    name = models.CharField(max_length = 100, blank = True, null = True)
    value = models.CharField(max_length = 100, blank = True, null = True)
    day = models.IntegerField(blank = True, null = True)

    file = models.FileField(upload_to='uploads/', blank = True, null = True)

    session = models.ForeignKey(CallSession, on_delete=models.CASCADE, related_name="session")
    user = models.ForeignKey(KasaDakaUser, on_delete=models.CASCADE, related_name="user", null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Chicken batch"
        verbose_name_plural = "Chicken batches"

def lookup_or_create_result(session, result_name, value = ''):

    result, created = Result.objects.get_or_create(None, name=result_name, session_id = session.id, day=1)

    if created:
        result.session = session
        result.value = value

        if session.user:
            result.user = session.user

        result.save()

    return result
