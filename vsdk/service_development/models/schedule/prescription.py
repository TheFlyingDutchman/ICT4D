from django.db import models
from ..voicelabel import VoiceLabel
from ..viewonly import ViewPermissionsMixin

class Prescription(ViewPermissionsMixin):
    voice_label = models.ForeignKey(VoiceLabel)

    def __str__(self):
        return "'%s'" % self.voice_label.name

    class Meta:
        verbose_name = "Disease"
        verbose_name_plural = "Diseases"
