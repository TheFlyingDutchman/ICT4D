# Credits for this file go to "group 1",
# code from: https://github.com/siaws2001/KasaDaka-VSDK/

from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect

from ..models import *
from ..models.schedule import *

def get_prescriptions_of_user(session):
    user = lookup_kasadaka_user_by_caller_id(session.caller_id, None)
    results = Result.objects.filter(user=user)

    recordings = {}
    day_prescriptions = {}

    # Get all the prescriptions for the current results (batches)
    for result in results:
        try:
            day = Day.objects.get(day_number=result.day)
            # Add the prescriptions for the current day to a dict, so that other
            # results with the same day don't have to query for these prescriptions again
            if not day.day_number in day_prescriptions:
                day_prescriptions[day.day_number] = []
                for prescription in day.prescriptions.all():
                    day_prescriptions[day.day_number].append(prescription.voice_label.get_voice_fragment_url(session.language))

            if len(day_prescriptions[day.day_number]):
                recordings[result.file.url] = day_prescriptions[day.day_number]
        except Day.DoesNotExist:
            continue # Nothing to be done for this batch on this day

    return recordings

def reminder_generate_context(reminder_element, session):
    language = session.language

    prescriptions_for_batch_label = reminder_element.prescriptions_for_batch_label.get_voice_fragment_url(language)
    hear_or_skip_label = reminder_element.hear_or_skip_label.get_voice_fragment_url(language)
    vaccinate_batch_label = reminder_element.vaccinate_batch_label.get_voice_fragment_url(language)
    for_diseases_label = reminder_element.for_diseases_label.get_voice_fragment_url(language)
    hear_again_or_continue_label = reminder_element.hear_again_or_continue_label.get_voice_fragment_url(language)
    final_label = reminder_element.final_label.get_voice_fragment_url(language)
    results = get_prescriptions_of_user(session)

    context = {'prescriptions_for_batch_label': prescriptions_for_batch_label,
               'hear_or_skip_label': hear_or_skip_label,
               'vaccinate_batch_label': vaccinate_batch_label,
               'for_diseases_label': for_diseases_label,
               'hear_again_or_continue_label': hear_again_or_continue_label,
               'final_label': final_label,
               'results': results,
               'num_results': len(results)
               }

    return context

def reminder(request, element_id, session_id):
    reminder_element = get_object_or_404(Reminder, pk=element_id)
    session = get_object_or_404(CallSession, pk=session_id)
    # user_voice_service = get_object_or_404(VoiceService, pk=name)
    print(session.caller_id, session.service)
    # print(session.service.user_base_voice_service)

    session.record_step(reminder_element)
    context = reminder_generate_context(reminder_element, session)

    print(context)

    return render(request, 'reminder.xml', context, content_type='text/xml')
